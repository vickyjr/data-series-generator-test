# README #

This is to help you set up and review test completed by me (Victor Karanja) issued by Gro Intelligence to prove capability of accomplishing various front end development tasks. The project uses best front-end development practices and modern technologies.

### Accomplished ###

* PSD to HTML5 and CSS convertion
* Responsiveness of the UI
* Custom developed componenets

### How to run it ###

* Download of pull the source files from this repo
* open the index.html file in a browser
* Interact with all components - expect permutations and combinations generated

### Dependancies ###
* Bootstap
* JQuery
* Angular 
* Checklist-model

### Technologies and Frameworks ###
* HTML5
* CSS3
* Bootstap
* JQuery
* Bower
* Sass
* Angular
