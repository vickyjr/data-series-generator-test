var seriesWidgetApp = angular.module('seriesWidgetApp', ['checklist-model']);

seriesWidgetApp.controller('seriesWidgetCtrl', function ($scope) {
    $scope.countries = [
        'Kenya',
        'Egypt',
        'Ethiopia'
    ];

    $scope.crops = [
        'Corn',
        'Oat',
        'Wheat'
    ];

    $scope._countries = {
        selected: []
    };

    $scope._crops = {
        selected: []
    };

    $scope.combineregions = false;
    $scope.combinecrops = false;

    $scope.permutations = [];

    $scope.iscountries = $scope._countries.selected.length > 0 ? false : true;

    $scope.isCountriesEmpty = function () {
        $scope.iscountries = $scope._countries.selected.length > 0 ? false : true;
    }

    $scope.iscrops = $scope._crops.selected.length > 0 ? false : true;

    $scope.isCropsEmpty = function () {
        $scope.iscrops = $scope._crops.selected.length > 0 ? false : true;
    }

    $scope.$watchCollection('_countries.selected', function () {
        $scope.isCountriesEmpty();
    });

    $scope.$watchCollection('_crops.selected', function () {
        $scope.isCropsEmpty();
    });

    $scope.generate = function () {
        $scope.permutations = [];
        var seperator = '';
        var countries = '';
        var crops = '';

        if ($scope.combineregions == false && $scope.combinecrops == false) {
            for (var i = 0; i < $scope._countries.selected.length; i++) {
                for (var j = 0; j < $scope._crops.selected.length; j++) {
                    $scope.permutations.push({ country: $scope._countries.selected[i], crop: $scope._crops.selected[j] });
                }
            }
        } else if ($scope.combineregions == true && $scope.combinecrops == true) {
            

            for (var i = 0; i < $scope._countries.selected.length; i++) {

                seperator = i > 0 ? ' + ' : ' ';

                countries += seperator + $scope._countries.selected[i];
            }

            

            for (var j = 0; j < $scope._crops.selected.length; j++) {

                seperator = j > 0 ? ' + ' : ' ';

                crops += seperator + $scope._crops.selected[j];
            }

            $scope.permutations.push({ country: countries, crop: crops });

        } else if ($scope.combineregions == true) {
            for (var i = 0; i < $scope._countries.selected.length; i++) {

                seperator = i > 0 ? ' + ' : ' ';

                countries += seperator + $scope._countries.selected[i];
            }

            for (var j = 0; j < $scope._crops.selected.length; j++) {
                $scope.permutations.push({ country: countries, crop: $scope._crops.selected[j] });
            }
        } else if ($scope.combinecrops == true) {

            for (var j = 0; j < $scope._crops.selected.length; j++) {

                seperator = j > 0 ? ' + ' : ' ';

                crops += seperator + $scope._crops.selected[j];
            }

            for (var i = 0; i < $scope._countries.selected.length; i++) {
                $scope.permutations.push({ country: $scope._countries.selected[i], crop: crops });
            }
        }
    }
});


seriesWidgetApp.directive('selectArrows', [function () {
    function link(scope, elem, attrs) {
        elem.bind('click', function () {
            elem.toggleClass('open');
        })
    }

    return {
        restrict: 'C',
        link: link
    }
}]);